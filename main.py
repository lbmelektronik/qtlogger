import multiprocessing 
from PyQt5.QtCore import *
from PyQt5.QtGui import QCursor 
from PyQt5 import QtWidgets, uic 
from PyQt5.QtWidgets import QMessageBox, QFileDialog, QLabel, QApplication
import pyqtgraph as pg
import pyqtgraph.exporters
import sys
import loggerCom as lc
from time import time, strftime,localtime, sleep
import datetime
import numpy as np
import json
import h5py
import webbrowser
import os
import camera
import reportGen


PROG_VER = '1.2.0' 
BACKUP_TIME = 600 # Seconds
UPDATE_CURVE_EVERY_10_SAMPLES_START = 200
AUTOPAN_X_RANGE = 60

class MainWindow(QtWidgets.QMainWindow):

	def __init__(self, *args, **kwargs):
		super(MainWindow, self ).__init__(  *args, **kwargs)

		self.serialComQueue = multiprocessing.Queue()
		self.guiQueue = multiprocessing.Queue()
		self.work = multiprocessing.Process(target=lc.SerialCom,args=(self.guiQueue, self.serialComQueue) ) # Process to receive data from
		self.work.daemon = True
		self.work.start()		
		#Load the UI Page
		uic.loadUi('mainwindow.ui', self)
		self.aliasDialog = uic.loadUi('aliasDialog.ui')
		self.reportDialog = uic.loadUi('report.ui')

		# GUI connections
		self.menuExit.triggered.connect(self.closeEvent)
		self.menuSaveAlias2Logger.triggered.connect(self.saveAliasSlot)
		self.menuLoadAlias.triggered.connect(self.readAliasSlot)
		self.menuSaveTempData.triggered.connect(self.saveDataSlot)
		self.menuLoadTempData.triggered.connect(self.loadDataSlot)
		self.menuResetLoggingData.triggered.connect(self.resetLoggingDialogSlot)
		self.menuPauseLogging.triggered.connect(self.pauseLoggingSlot)
		self.menuAbout.triggered.connect(self.aboutDialogSlot)
		self.menuBackgroundBlack.triggered.connect(self.backgroundBlackSlot)
		self.menuBackgroundWhite.triggered.connect(self.backgroundWhiteSlot)
		self.menuVoltExportCsvWindow.triggered.connect(self.csvExportVoltWindowSlot)
		self.menuVoltExportCsvAll.triggered.connect(self.csvExportVoltSlot)
		self.menuTempExportCsvWindow.triggered.connect(self.csvExportTempWindowSlot)
		self.menuTempExportCsvAll.triggered.connect(self.csvExportTempSlot)
		self.menuCamExportCsvWindow.triggered.connect(self.csvExportCamWindowSlot)
		self.menuCamExportCsvAll.triggered.connect(self.csvExportCamSlot)
		self.menuTempSortMax.triggered.connect(self.menuTempSortMaxSlot)
		self.menuTempSortMin.triggered.connect(self.menuTempSortMinSlot)
		self.menuTempSortReset.triggered.connect(self.menuTempSortResetSlot)
		self.menuCamSortMax.triggered.connect(self.menuCamSortMaxSlot)
		self.menuCamSortMin.triggered.connect(self.menuCamSortMinSlot)
		self.menuCamSortReset.triggered.connect(self.menuCamSortResetSlot)
		self.menuRestoreAutoRange.triggered.connect(self.menuRestoreAutoRangeSlot)
		#self.camDialog.buttonBox.clicked.connect(self.menuCamComRespons)
		self.menuCursorTempMeasure.triggered.connect(lambda: self.menuCursorMeasureSlot('t') )
		self.menuCursorVoltMeasure.triggered.connect(lambda: self.menuCursorMeasureSlot('v') )
		self.menuCursorCameraMeasure.triggered.connect(lambda: self.menuCursorMeasureSlot('c') )		
		self.menuFanOffComp.triggered.connect(self.menuFanOffCompSlot)
		self.menuHelp.triggered.connect(self.help)
		self.menuDeltaTempComp.triggered.connect(self.menuDeltaTempCompSlot)
		self.menuReport.triggered.connect(self.menuReportSlot)
		self.menuSaveAliasToFile.triggered.connect(self.saveAliasToFileSlot)
		self.menuLoadAliasFromFile.triggered.connect(self.loadAliasFromFileSlot)
		self.menuTempExportImage.triggered.connect(lambda: self.menuExportImageSlot('t'))
		self.menuCameraExportImage.triggered.connect(lambda: self.menuExportImageSlot('c'))
		self.menuVoltExportImage.triggered.connect(lambda: self.menuExportImageSlot('v'))
		self.camAllBtn.clicked.connect(self.camBtnSlot)
		self.reportDialog.buttonBoxReport.clicked.connect(self.reportDialogSlot)	# Report dialog
		
		self.tempPlotWidget.sigXRangeChanged.connect(self.xaxisChangedSlot)
		self.tempPlotWidget.showGrid(x=True, y=True)
		self.tempAllButton.clicked.connect(self.tempBtnSlot)	
		self.tempPlotWidget.setAxisItems({'bottom': TimeAxisItem(orientation='bottom')}) #pg.DateAxisItem()})
		self.voltAllButton.clicked.connect(self.voltBtnSlot)
		self.voltPlotWidget.setAxisItems({'bottom': TimeAxisItem(orientation='bottom')}) #pg.DateAxisItem()})
		self.voltPlotWidget.showGrid(x=True, y=True)
		self.aliasDialog.buttonBox.clicked.connect(self.aliasEditDialogRespons) #All buttons in Alias dialog
		self.plotWindowScrollBar.sliderMoved.connect(self.plotWindowScrollBarSlot)	#Change relative size of volt and temp plot
		self.autoPanCheckBox.clicked.connect(self.autoPanTempCBSlot)	
		self.sampleTimeSlider.valueChanged.connect(self.updateSampleTimeLabelSlot)
		self.updateSampleTimeLabelSlot(1) # Fast start
		self.autoPan = False
		
		self.connectorBanks =['A','B','C','D','X']
		self.colorList =[[100,50,0],[255,0,0], [255,170,0], [255,255,0],[0,255,0],[0,0,255],[170,0,255],[136,142,147]] # Brown, red, orange, yellow, green, blue, purple, gray
		self.colorListCb =['190,90,0','255,0,0', '255,170,0', '255,255,0','0,255,0','124,124,255','240,160,230','136,142,147'] # Brown, red, orange, yellow, green, blue, purple, gray
		self.lineStyleList = [Qt.SolidLine, Qt.DashLine, Qt.DotLine,  Qt.DashDotLine, Qt.DashDotDotLine]
		self.progressBar.setValue(0)
		# Data structure
		self.ld={}
		for channelType in ['t','v','c']:
			# Dictionary for a logger type (voltage, temp)
			self.ld[channelType]={'channelList': [],
				'signalMapper' : QSignalMapper(self), 
				'toggleAllEnable' : False,
				'xData': [], 
				'xStart': 0,	# Graph window start point
				'sampleCount':0,
				'forceUpdate': False,
				'channelCount': 0,
				'auotPan': False,	
				'aliasDict': 0,
				'aliasProcessed': 'None',
				'nextSampleTime' : time(),
				'eventList' : [],
				'name' : ''}
		self.ld['t']['plotWidget']= self.tempPlotWidget
		self.ld['t']['selectWidget']= self.selectTempWidget
		self.ld['t']['selectWidgetSlot']= self.tempSelectSlot	# Note function, not widget
		self.ld['t']['name'] = "Temp"
		self.ld['v']['plotWidget']= self.voltPlotWidget
		self.ld['v']['selectWidget']= self.selectVoltWidget
		self.ld['v']['selectWidgetSlot']= self.voltSelectSlot	# Note function, not widget
		self.ld['v']['name'] = "Volt"
		self.ld['c']['plotWidget']= self.tempPlotWidget
		self.ld['c']['selectWidget']= self.selectCamWidget
		self.ld['c']['selectWidgetSlot']= self.tempSelectSlot	# Note function, not widget
		self.ld['c']['name'] = "Camera"
		
		self.loggingPaused = False
		self.statusBar().showMessage("Start up")
		self.startLoggingTime = time()
		
		self.timer1 = QTimer()
		self.timer1.timeout.connect(self.queueWorker)
		self.timer1.start(500)
		self.enableBackup = True # Timely save all login data to disk
		self.nextBackupTime = time() + BACKUP_TIME
		self.backupCounter = 0
		self.backupFileNameStr = strftime("%y%m%d-%H%M-", localtime())		
		self.menuCamComRespons() # Ask for Camera (IP)
		self.mouseClickedInGraphConnected = None
		
		self.ipAddress = 'None'
		self.cameraMacAddress = ''
		self.cameraName = 'None'
		self.cameraProgVer = 'None'
		self.logDate = strftime("%Y-%m-%d %H:%M:%S", localtime())
		#self.cursor = None
		#
		# Setup cursor measurement
		#
		self.cursorWindow = CursorMeasure()		# Create cursor measure window
		self.cursorWindow.hideWin()

		self.cursorTemp = pg.InfiniteLine(pos = 0, angle=90, movable=True, label='Measure', pen=pg.mkPen(color=self.colorList[4], width=3), labelOpts={'movable' : True})
		self.cursorTemp.addMarker(marker='>|<',position= 0.45, size=10)
		self.ld['t']['plotWidget'].addItem(self.cursorTemp)
		self.cursorTemp.hide()
		self.cursorTemp.sigPositionChangeFinished.connect (self.cursorPositionChanged)
		
	
		
		
		#self.cursorTemp.sigDragged.connect (self.cursorPositionChanged)
		#self.cursorTemp.sigPositionChanged.connect (self.cursorPositionChanged)
		
		self.cursorVolt = pg.InfiniteLine(pos = 0, angle=90, movable=True, label='Measure', pen=pg.mkPen(color=self.colorList[4], width=3), labelOpts={'movable' : True})
		self.cursorVolt.addMarker(marker='>|<',position= 0.45, size=10)
		self.ld['v']['plotWidget'].addItem(self.cursorVolt)
		self.cursorVolt.hide()
		self.cursorVolt.sigPositionChangeFinished.connect (self.cursorPositionChanged)
		
	def disableVirtualGnd (self):
		''' Last channel in volt deck 2-4 is used as ground for its own deck (configured in logger). This channels can't be selected'''
		for x in [15,23,31]:	# Disable all virtual GNDs
			self.ld['v']['channelList'][x]['checkBox'].setEnabled(False)
			self.ld['v']['channelList'][x]['alias']='Virt GND'
			self.ld['v']['channelList'][x]['checkBox'].setStyleSheet("background-color: rgb(255,255,255")

	def createChannel(self, channelType, channelNo):
		'''| Returns a channel for temperature or voltage measurement
		| A channel consist of
		| - Data dictionary; y array, min, max etc
		| - Plot line
		| - Enable Checkbox'''
		# channel dictionary
		d = {'checkBox' : QtWidgets.QCheckBox('-'), 
			'label' : (self.connectorBanks[int(channelNo/8)]+str(channelNo%8+1) ), # C-box labeled A1..D8
			'channelNo' :channelNo, 
			'max' : -99, 
			'min' : 900, 
			'yData' :np.zeros(300000), 
			'dataLine' : 0,
			'alias' :'-',
			'compMaxTemp': 0,	# Max measured temp during fan off temp compensation phase
			'compensatedMaxTemp': 0,
			'compState': 'off'}	# TC state machine
		d['checkBox'].setText('{0:3.2} {1:11.10}'.format(d['label'], d['alias']))
		d['checkBox'].setStyleSheet("background-color: rgb(" +self.colorListCb[d['channelNo']%8] + ")")
		d['checkBox'].setChecked(True)	# Make all channels available when Load Data caommand
		d['checkBox'].clicked.connect(self.ld[channelType]['signalMapper'].map)		# Connect checkbox to mapper (not slot)
		self.ld[channelType]['signalMapper'].setMapping(d['checkBox'] , channelNo) 
		self.ld[channelType]['selectWidget'].addWidget(d['checkBox'])	# Add widget to screen
		d['dataLine'] = self.ld[channelType]['plotWidget'].plot([0,],[0,], pen=pg.mkPen(color = self.colorList[d['channelNo'] % 8], style = self.lineStyleList[int(d['channelNo']/8)], width=2 ), autoDownsample = True )
		
		return d
	
	def createAllChannels(self,sStr, channelType):
		'''| Create connected channels camera or logger	
		| Connect mouse clicks in graph to slot (not from channel select list. 
		| Note for temp graph, this can either be temp or camera curve, first setup wins
		| Adding an event/arrow in graph, connects to the first channel'''		
		self.ld[channelType]['channelList'] =[]	# Todo: remove?? chcedk w load save data
		self.ld[channelType]['channelCount'] = 0	# Physical channel on logger	 Todo: remove??
		for yStr in sStr:
			y = float(yStr)
			if ((y > -99) and (y < 999) ): # Sensor connected
				self.ld[channelType]['channelList'].append(self.createChannel(channelType, self.ld[channelType]['channelCount']))
				#self.ld['t']['channelList'][-1]['yData'][self.ld['t']['sampleCount']]=float(yStr ) 
			self.ld[channelType]['channelCount'] += 1		
		self.ld[channelType]['signalMapper'].mapped['int'].connect(self.ld[channelType]['selectWidgetSlot'])
		#self.ld['t']['xData'].append(time()-self.startLoggingTime) 
		#self.ld['t']['sampleCount'] += 1	
		self.menuLoadTempData.setEnabled(False) # Not possible to append data to running execution
		self.menuSaveTempData.setEnabled(True) 
		if self.ld['t']['channelCount'] > 0 and self.ld['t']['aliasProcessed'] == 'None':
			self.serialComQueue.put(['logReadTempAlias',])
			self.ld['t']['aliasProcessed'] = 'Requested'
			if self.mouseClickedInGraphConnected is None:
				self.ld['t']['channelList'][0]['dataLine'].scene().sigMouseClicked.connect(self.mouseClickedInGraph)  
				self.mouseClickedInGraphConnected = 't'
		if self.ld['v']['channelCount'] > 0 and self.ld['v']['aliasProcessed'] == 'None':
			self.serialComQueue.put(['logReadVoltAlias',])
			self.ld['v']['aliasProcessed'] = 'Requested'
			self.ld['v']['channelList'][0]['dataLine'].scene().sigMouseClicked.connect(self.mouseClickedInGraph)
		if channelType == 'c':
			self.camCmdQueue.put('getAlias')
			self.camCmdQueue.put('getMacAndId')
			if self.mouseClickedInGraphConnected is None and self.ld['c']['channelCount'] > 0 :
				self.ld['c']['channelList'][0]['dataLine'].scene().sigMouseClicked.connect(self.mouseClickedInGraph)  
				self.mouseClickedInGraphConnected = 'c'
		if self.ld['v'] and self.ld['v']['channelCount'] == 32: # It's required to get all channels from Voltage
			self.disableVirtualGnd()
			
	def updatePlot(self,ch, channelType):
		'''| Update plot and select list (checkboxes)
		| Input: ch: channel dic, channelType: 't' or 'v' 
		| Update used-memory-progressbar'''
		ch['checkBox'].setText('{0:3.2}{1:6.2f}  {2:6.2f}  {3:6.2f} {4:11.10}'.format(ch['label'],ch['min'], ch['yData'][self.ld[channelType]['sampleCount']], ch['max'], ch['alias']))
		if not ch['checkBox'].checkState()  or (ch['channelNo'] in (15,23,31) and channelType == 'v') : # Line disabled
			ch['dataLine'].setVisible(False) 
			return 
		startX = 0 
		#if self.autoPan:	# Show latest data part
		#	startX = max (0,self.ld[channelType]['sampleCount']-AUTOPAN_X_RANGE)
		ch['dataLine'].setData(self.ld[channelType]['xData'][startX:], ch['yData'][startX:self.ld[channelType]['sampleCount']] )
		ch['dataLine'].setVisible(True)
		# Update memory used progressbar
		try:
			t = self.ld['t']['sampleCount']
		except:
			t = 0
		try:
			v = self.ld['v']['sampleCount']
		except:
			v = 0	
		try:
			c = self.ld['c']['sampleCount']
		except:
			c = 0
		self.progressBar.setValue(int(max(t, v, c)/300000*100))
			
	def plot(self, dataStr):
		'''| Plot all enabled channels in data string. If first reading, create and enable all available channels for temp.
		| Input dataStr: data from logger	
		| Note 1: When using cursors the auto pan x range will include cursor positions, which may be out of auto pan area, if not limited,
		| Note 2: If both Temp and Camera use temp graph, the x axis change back and forward dependant on source. Source limited
		'''	
		channelType = dataStr[:1].lower()	# Extract volt or temp command
		dataStr = dataStr[1:].split(',')	# Skip t or v char
		if (self.ld[channelType]['channelCount'] == 0) :	# Create channel struct if first sample
			self.createAllChannels(dataStr, channelType)
			return
		#
		# Get next time to sample. 
		#
		t = time()	# If log time > sample time, a backlog will build up. Shouldn't cause any problem
		if t > self.ld[channelType]['nextSampleTime']:		
			self.ld[channelType]['nextSampleTime'] = t + self.sampleTime 
			updateEveryNCycle = ((self.ld[channelType]['plotWidget'].viewRange()[0][1] - self.ld[channelType]['plotWidget'].viewRange()[0][0]) > UPDATE_CURVE_EVERY_10_SAMPLES_START) # ViewRange[0] == X range. On large plots, plot only every 10th 
			
			for ch in self.ld[channelType]['channelList']:
				y = float(dataStr[ch['channelNo'] ])
				ch['max'] = max(y,ch['max'])
				ch['min'] = min(y,ch['min'])
				ch['yData'][self.ld[channelType]['sampleCount']] =y
				#if ((self.ld[channelType]['plotWidget'].viewRange()[0][1] - self.ld[channelType]['plotWidget'].viewRange()[0][0]) > UPDATE_CURVE_EVERY_10_SAMPLES_START): # ViewRange[0] == X range. On large plots, plot only every 10th 
				if updateEveryNCycle:
					if (self.ld[channelType]['sampleCount'] % 10 == 0)  or self.ld[channelType]['forceUpdate']:
						self.ld[channelType]['plotWidget'].enableAutoRange(y = False)
						self.updatePlot(ch,channelType)
				else:
					self.updatePlot(ch,channelType)
					
			self.ld[channelType]['xData'].append(int(time()-self.startLoggingTime))
			self.ld[channelType]['sampleCount'] += 1
			self.ld[channelType]['forceUpdate'] = False
			#
			# Time for backup?
			#
			if (t > self.nextBackupTime) and self.enableBackup:
				self.backupCounter += 1
				self.nextBackupTime += BACKUP_TIME
				fName = "data/"+self.backupFileNameStr+"{}".format(self.backupCounter % 2)+".hdf5"
				#print ("Before saveData",time())
				self.saveData(fName)
				#print ("After saveData",time())
			# 
			# Auto pan
			#
			if self.autoPan:	 
				if channelType == 'c' and self.ld['t']['sampleCount'] > 0:
					channelType = 't' # Limit source, see comment above
				self.ld[channelType]['plotWidget'].setXRange( # Value, not index!
					self.ld[channelType]['xData'][self.ld[channelType]['sampleCount']-1]-AUTOPAN_X_RANGE, 
					self.ld[channelType]['xData'][self.ld[channelType]['sampleCount']-1] )	
				
	def enableAllChannels(self, channelType):
		'''| Select or unselect ALL checkboxes 
		| Input: channelType: 't', 'c' or 'v' '''	
		try:
			for ch in self.ld[channelType]['channelList']:
				ch['checkBox'].setChecked(self.ld[channelType]['toggleAllEnable'])
				self.updatePlot(ch, channelType)
			self.ld[channelType]['toggleAllEnable']	= not self.ld[channelType]['toggleAllEnable']	
			self.ld[channelType]['forceUpdate']
		except:
			pass # 'c' not ready yet
		
	def tempBtnSlot(self): # modify for available boxes
		self.enableAllChannels('t')


	def voltBtnSlot(self):
		self.enableAllChannels('v')

	def camBtnSlot(self):
		self.enableAllChannels('c')

	
	def channelSelect(self, channel, channelType):
		'''| Slots for check / uncheck single enable boxes. 
		| Input: channel: index in channel list
		| Input: channelType: 't' for temperature, 'v' for voltage and 'c' for camera'''
		self.ld[channelType]['forceUpdate']=True
		for ch in self.ld[channelType]['channelList']:
			if ch['channelNo'] == channel:
				self.updatePlot(ch, channelType)
				break
			
	def tempSelectSlot(self, channel):
		'''| Slot for check / uncheck single temperature enable boxes.
		| Input: channel: index in channel list''' 	
		self.channelSelect(channel,'t')
		
	def voltSelectSlot(self, channel):
		'''| Slot for check / uncheck single voltage enable boxes.
		| Input: channel: index in channel list''' 	
		self.channelSelect(channel,'v')

	def aboutDialogSlot(self):
		''' Help / About'''
		QMessageBox.information(self,"Qt logger", 'Version {:}\n\nMaintainer: Lars Eriksson'.format(PROG_VER) )
		
	def resetLoggingDialogSlot(self):
		'''| File / Reset all logging data
		| Clear all saved logging data'''
		if QMessageBox.critical(self, 'Warning', 'Reset all temperature and voltage data?\n(Alias preserved)', QMessageBox.Yes | QMessageBox.No, QMessageBox.No ) == QMessageBox.Yes:
			self.ld['t']['sampleCount']= 0
			self.ld['v']['sampleCount']= 0
			self.ld['t']['xData'] = []
			self.ld['v']['xData'] = []
			if 'c' in self.ld:
				self.ld['c']['sampleCount']= 0
				self.ld['c']['xData'] = []
		# Todo: remove Events from tab and channelTypes eventList
			
	def pauseLoggingSlot(self):
		''' File / Pause logging'''
		self.loggingPaused = True
		QMessageBox.warning(self, 'Logging stopped', 'Press OK to continue logging', QMessageBox.Ok)
		self.loggingPaused = False

			
	def plotWindowScrollBarSlot(self, val):
		'''This Slot change the relation between the temp and volt window height. Connected to vertical slider'''
		self.plotWindowsLayout.setRowStretch(0,val)
		self.plotWindowsLayout.setRowStretch(1,10-val)
		
	def updateSampleTimeLabelSlot(self, value):
		self.sampleTimeLabel.setText("{} Seconds/sample".format(value))
		self.sampleTime = value
		try:
			self.camCmdQueue.put(['setSampleTime',value])
		except:
			pass # Cam may not be started yet
	
	def populateAlias(self,channelType, alias):
		''' Process loaded alias for one type (t or v) either from logger or file
		'''
		self.ld[channelType]['aliasProcessed'] = 'Processed'
		if 1 >= len(alias):
			self.statusBar().showMessage("No alias files")
		else:
			try:
				self.ld[channelType]['aliasDict'] = alias #json.loads(alias)
				for label,txt in self.ld[channelType]['aliasDict'].items():	# Secondary loop
					for tCh in self.ld[channelType]['channelList']: #find current label
						if label == tCh['label']:
							if channelType == 't':
								self.aliasDialog.aliasTempList.insertPlainText('{0:15.17}{1:15}\n'.format(label,txt)) 
							else:
								self.aliasDialog.aliasVoltList.insertPlainText('{0:15.17}{1:15}\n'.format(label,txt)) 
							break
			except:
				self.statusBar().showMessage("Error in alias file")
			
	def queueWorker(self):
		'''Read the queue from logger hardware or camera. Called by timer.	'''
		while not self.guiQueue.empty():
			command = self.guiQueue.get()
			#print (command[0])
			if command[0] == "msg":
				self.statusBar().showMessage(command[1])
			if command[0] == "plot":
				if (command[1][0] == 'T' or command[1][0] == 'V')and not self.loggingPaused:
					self.plot(command[1])					
			if command[0] == 'tempAlias' and len(command[1]) > 10:
				self.aliasDialog.aliasTempList.clear()
				self.populateAlias('t', json.loads(command[1]))
				if self.ld['v']['aliasProcessed'] == 'Processed': # Both temp and volt alias processed
					self.aliasDialog.show() 

			if command[0] == 'voltAlias' and len(command[1]) > 10:
				self.aliasDialog.aliasVoltList.clear()
				self.populateAlias('v', json.loads(command[1]))
				if self.ld['t']['aliasProcessed'] == 'Processed': # Both temp and volt alias processed
					self.aliasDialog.show() 

			if command[0] == 'logRemoveTempAlias':
				self.statusBar().showMessage("Remove alias file not implemented yet")
			
			if command[0] == "internal temps":	# Temps from camera?
				# Start on channel 33 to avoid kollision with Temp logger
				c = "C1000,1000,1000,1000,1000,1000,1000,1000,1000,1000,1000,1000,1000,1000,1000,1000,1000,1000,1000,1000,1000,1000,1000,1000,1000,1000,1000,1000,1000,1000,1000,1000"
				labels=[]
				for key, val in command[1].items():
					labels.append(str(key))
					c = c + "," + val
				self.plot(c)
			if command[0] == 'camAlias':
				self.setCamAlias(command[1])
			
			if command[0] == 'getMacAndId':
				self.cameraMacAddress = command[1][0]
				self.cameraName = command[1][1]
				self.cameraProgVer = command[1][2]
				
	def closeEvent(self, event):	# Illegal warning!
		''' Close logger application '''
		self.serialComQueue.put('exit')
		sleep(0.5)
		exit()
	
	def autoPanTempCBSlot(self, checked):
		'''| Auto pan checkbox slot
		| Checked: Boolean from checkbos status'''
		self.autoPan = checked
		if checked == False:
			self.menuRestoreAutoRangeSlot()
		self.ld['t']['forceUpdate']= True
		self.ld['v']['forceUpdate']= True

	def backgroundBlackSlot(self):
		'''| Change graph backgrounds color to black '''
		self.ld['t']['plotWidget'].setBackground("k")
		self.ld['v']['plotWidget'].setBackground("k")
		
	def backgroundWhiteSlot(self):
		'''| Change graph backgrounds color to white '''
		self.ld['t']['plotWidget'].setBackground("w")
		self.ld['v']['plotWidget'].setBackground("w")
	
		
	def mousePressEvent(self, event):	
		'''| Right mouse button used to edit channel alias 
		| Note that Shift or Ctrl Right button is blocked by default plotWidget Right button ignore modifiers!
		'''
		pos = QCursor.pos()	
		widget = QtWidgets.QApplication.widgetAt(pos)
		if event.buttons() == Qt.RightButton:
			try:	# If Right Mouse Button on check box
				t = widget.text()
				if widget.parent().objectName() == 'scrollTempWidget':	# Choose temp or volt labels, since the labels have the same name
					for tCh in self.ld['t']['channelList']:
						if t[:2] == tCh['label']:
							txt, ok = QtWidgets.QInputDialog.getText(self, '','Input Temperature Alias\n\nNo Swedish characters!\n\n'+tCh['label'])
							if ok==True:
								tCh['alias'] = txt
							break
				if widget.parent().objectName() == 'scrollVoltWidget':
					for vCh in self.ld['v']['channelList']:
						if t[:2] == vCh['label']:
							txt, ok = QtWidgets.QInputDialog.getText(self, '','Input Voltage Alias\n\nNo Swedish characters!\n\n'+vCh['label'])
							if ok==True:
								vCh['alias'] = txt
							break
				event.accept()
			except Exception as e: 
				print ("Exception right mouse button:", e)
	
	def mouseClickedInGraph(self, point):
		'''| Handle middle mouse clicke in plot widgets.
		| Add event to t,v or c at the cursor position and add to event list.
		'''
		
		if point.buttons() == Qt.MiddleButton:
			pos = QCursor.pos()
			widget = QApplication.widgetAt(pos)
			print (widget.parent().objectName())
			# Get which plot widget has the cursor 
			if 'tempPlotWidget' == widget.parent().objectName():
				channelType = self.mouseClickedInGraphConnected		# Either 'c' or 't'	
				labelHeader="Temp "			
			if 'voltPlotWidget' == widget.parent().objectName():
				channelType = 'v'
				labelHeader="Volt   "
			
			txt, ok = QtWidgets.QInputDialog.getText(self, '','Input Event\n\nNo Swedish characters!\n')	
			if ok==True:
				p =self.ld[channelType]['plotWidget'].plotItem.vb.mapSceneToView(point.scenePos()) 
				xValue = p.x()
				i = pg.InfiniteLine(pos = xValue, angle=90, movable=False, label=txt) #,pen=pg.mkPen(color=self.colorList[4], width=5)
				self.ld[channelType]['plotWidget'].addItem(i)
				
				label = labelHeader+" {}-{:02d}:{:02d}:{:02d} ".format(int(xValue/86400),int(xValue/3600) % 24, int(xValue/60) % 60, int(xValue) % 60) + txt
				self.eventListWidget.addWidget(QLabel(label))
				print ("event", label)
				self.ld[channelType]['eventList'].append([str(xValue),label])	# Str(l): h5py is easier to deal with if same data type in list
				self.ld[channelType]['forceUpdate'] = True
			point.accept()
				
	def aliasEditDialogRespons(self, button):
		'''Alias dialog Accept, reject or discard Alias from logger'''		
		if button.text() == '&Yes':
			if self.ld['t']['aliasDict']:
				for label,txt in self.ld['t']['aliasDict'].items():	# Temp alias
					for tCh in  self.ld['t']['channelList']: #find current label
						if label == tCh['label']:
							tCh['alias'] = txt
							break
			if self.ld['v']['aliasDict']:
				for label,txt in self.ld['v']['aliasDict'].items():	# Volt alias
					for vCh in  self.ld['v']['channelList']: #find current label
						if label == vCh['label']:
							vCh['alias'] = txt
							break
		
		if button.text() == '&Discard':
			self.serialComQueue.put(['logRemoveTempAlias'])
		self.aliasDialog.hide()	# Yes and No button hide dialog. Discard doesn't
		self.ld['t']['forceUpdate']= True
		self.ld['v']['forceUpdate']= True

	def saveAliasSlot(self):
		'''| Save Alias to logger
		| Save both temp and voltage alias to logger'''
		alias={}
		for tCh in  self.ld['t']['channelList']:
			alias[tCh['label']]=tCh['alias']
		self.serialComQueue.put(['logSaveTempAlias',json.dumps(alias)])
		alias={}
		for vCh in  self.ld['v']['channelList']:
			alias[vCh['label']]=vCh['alias']
		self.serialComQueue.put(['logSaveVoltAlias',json.dumps(alias)])

	def saveAliasToFileSlot(self):
		options = QFileDialog.Options()
		options |= QFileDialog.DontUseNativeDialog
		filename, _ = QFileDialog.getSaveFileName(self,caption="Save Alias as", directory="reports",filter="json Files (*.json);;all files(*.*)")
		if filename:
			aliasVolt={}
			aliasTemp={}
			for tCh in  self.ld['t']['channelList']:
				aliasTemp[tCh['label']]=tCh['alias']
			for vCh in  self.ld['v']['channelList']:
				aliasVolt[vCh['label']]=vCh['alias']
				alias = {'t': aliasTemp, 'v' : aliasVolt}
			with open(filename, 'w') as f:
				json.dump(alias, f)
			
	def readAliasSlot(self):
		'''| Load Alias from logger
		| Send alias requests to logger'''
		self.ld['t']['aliasProcessed'] = False
		self.ld['v']['aliasProcessed'] = False
		self.serialComQueue.put(['logReadTempAlias',])
		self.serialComQueue.put(['logReadVoltAlias',])  

	def loadAliasFromFileSlot(self):
		self.aliasDialog.aliasTempList.clear()
		self.aliasDialog.aliasVoltList.clear()
		options = QFileDialog.Options()
		options |= QFileDialog.DontUseNativeDialog
		filename, _ = QFileDialog.getOpenFileName(self,caption="Load alias", directory="reports",filter="json Files (*.json);;all files(*.*)")
		if filename:
			f = open(filename)
			alias = json.load(f)
			self.ld['v']['aliasProcessed'] = 'None'
			self.ld['t']['aliasProcessed'] = 'None'
			self.populateAlias('t', alias['t'])
			self.populateAlias('v', alias['v'])
			self.aliasDialog.show() 
			
	def saveDataSlot(self):
		'''File Save data slot'''		
		options = QFileDialog.Options()
		options |= QFileDialog.DontUseNativeDialog
		fileName, _ = QFileDialog.getSaveFileName(self,caption="Save data as", directory="data",filter="hdf5 Files (*.hdf5);;all files(*.*)")
		if fileName:
			self.saveData(fileName)	
		
	def saveData(self,fileName):
		with h5py.File(fileName, 'w') as f:
			for channelType in self.ld:
				print ("save ", channelType)
				g = f.create_group(channelType)
				for tCh in self.ld[channelType]['channelList']:
					channel = f.create_group("{:s}/{:03d}".format(channelType,tCh['channelNo'])) #datasets are loaded in strict order (0,1,11,12,..19,2,20,21) therefor padded with zero
					channel.create_dataset('yData', data= tCh['yData'])
					channel.create_dataset('alias', data= tCh['alias'])
					channel.create_dataset('min', data= tCh['min'])
					channel.create_dataset('max', data= tCh['max'])
					channel.create_dataset('label', data= tCh['label'])
					#channel.create_dataset('eventList',   data = tCh['eventList'])
				g.create_dataset('xData', data= self.ld[channelType]['xData'])
				g.create_dataset('sampleCount', data = self.ld[channelType]['sampleCount'])
				g.create_dataset('eventList',   data = self.ld[channelType]['eventList'])
			f.create_dataset('ipAddress',   data = self.ipAddress)
			f.create_dataset('macAddress', data = self.cameraMacAddress)
			f.create_dataset('cameraName', data = self.cameraName)
			f.create_dataset('logDate', data = self.logDate)	
			
	def loadDataSlot(self):
		'''| File / Load data slot '''
		fName = QFileDialog.getOpenFileName(self, 'Open data file',   'data',"(*.hdf5)")
		self.loadData(fName[0])
		
	def loadData(self,fileName):
		'''| Loading log data from disk in HDF5 format
		| '''
		self.enableBackup = False # No saving of already saved data
		f = h5py.File(fileName, 'r')
		channelTypes=[]
		for c in f.keys():
			if c in ['c','t','v']:
				channelTypes.append(c)
			if c in ['c','t']:
				self.mouseClickedInGraphConnected = c	# Activate temp or cam cursor measurement
		for channelType in channelTypes:
			c = f.get(channelType)
			cName =str(c).split("\"")[1][1:]	# Skip trailing '/' 
			self.ld[channelType]['channelList'] =[]
			self.ld[channelType]['channelCount'] = 0
			self.ld[channelType]['xData'] = c['xData'][()] # f['key'] is a file object of none use | f['key'][()] is the actual data
			self.ld[channelType]['sampleCount'] = c['sampleCount'][()]
			for t in c: # for all channels in type
				if t.isnumeric():	# Channel dataset
					d = self.createChannel(cName, int(t))
					channel = f.get(cName+'/'+t)	
					d['yData'] = channel['yData'][()]
					d['alias'] = channel['alias'][()].decode('utf-8')
					d['label'] = channel['label'][()].decode('utf-8')
					d['min'] = channel['min'][()]
					d['max'] = channel['max'][()]
					#d['eventList'] = channel['eventList']
					self.ld[channelType]['channelList'].append(d)
					self.updatePlot(d, channelType)
					self.ld[channelType]['channelCount'] += 1
			self.ld[channelType]['signalMapper'].mapped['int'].connect(self.ld[channelType]['selectWidgetSlot'])
			self.ld[channelType]['eventList'] = c['eventList'][()]
			for el in self.ld[channelType]['eventList']: 	# Restore Event lines el[0] = xValue, el[1] = label
				self.eventListWidget.addWidget(QLabel(el[1].decode()))	
				i = pg.InfiniteLine(pos = float(el[0]), angle=90, movable=False, label=el[1][16:].decode()) #,pen=pg.mkPen(color=self.colorList[4], width=5)
				self.ld[channelType]['plotWidget'].addItem(i)
		self.ipAddress = f.get('ipAddress')[()].decode()		# For reports
		self.cameraMacAddress = f.get('macAddress')[()].decode()
		self.cameraName = f.get('cameraName')[()].decode()
		self.logDate = f.get('logDate')[()].decode()
		f.close()
		try:
			self.disableVirtualGnd()
		except:
			print ("Load data: No ad channels loaded")
		self.menuResetLoggingData.setEnabled(False)
		self.menuPauseLogging.setEnabled(False)
		self.menuLoadTempData.setEnabled(False) # Not possible to append another load to current
		self.autoPanCheckBox.setEnabled(False)
		self.sampleTimeSlider.setEnabled(False)
			
	def xaxisChangedSlot(self, e):
		'''| Slot sync volt x axis from temp x axis. 
		| Input: e: temp xaxis'''
		if self.syncCheckBox.isChecked():
			r = e.viewRange()
			self.ld['v']['plotWidget'].setXRange(r[0][0], r[0][1],padding=0)
			self.ld['t']['forceUpdate']= True
			self.ld['v']['forceUpdate']= True

	def csvExportTempWindowSlot(self):
		'''| Temp / Export / csv window
		| Export the displayed range to tab separated file'''
		x = self.ld['t']['plotWidget'].viewRange()[0] # Note this is not exactly displayed xmin and xmax
		xmin = max (int(x[0]),0)
		xmax = int(x[1])
		self.csvExportWindow('t', xmin, xmax)
		
	def csvExportCamWindowSlot(self):
		'''| Cam temp / Export / csv window
		| Export the displayed range to tab separated file'''
		x = self.ld['c']['plotWidget'].viewRange()[0] # Note this is not exactly displayed xmin and xmax
		xmin = max (int(x[0]),0)
		xmax = int(x[1])
		self.csvExportWindow('c', xmin, xmax)

	def csvExportVoltWindowSlot(self):
		'''| Cam temp / Export / csv window
		| Export the displayed range to tab separated file'''
		x = self.ld['v']['plotWidget'].viewRange()[0] # Note this is not exactly displayed xmin and xmax
		xmin = max (int(x[0]),0)
		xmax = int(x[1])
		self.csvExportWindow('v', xmin, xmax)

	def csvExportTempSlot(self):
		'''| Temp / Export / csv all
		| Export the displayed range to tab separated file'''
		self.csvExportWindow('t', 0,  self.ld['t']['channelCount'])
		
	def csvExportCamSlot(self):
		'''| Cam temp / Export / csv all
		| Export the displayed range to tab separated file'''
		self.csvExportWindow('c', 0,  self.ld['c']['channelCount'])

	def csvExportVoltSlot(self):
		'''| Temp / Export / csv all
		| Export the displayed range to tab separated file'''
		self.csvExportWindow('v', 0,  self.ld['v']['channelCount'])

	def csvExportWindow(self, param, xmin, xmax,  *args, **kwargs):
		'''| Worker file for export functions
		  | Input: Param: Parameter to export. xmin & xmax the range to export'''
		fileName = kwargs.get('fileName', None)
		#  get filename
		if fileName == None:
			options = QFileDialog.Options()
			options |= QFileDialog.DontUseNativeDialog
			fileName, _ = QFileDialog.getSaveFileName(self,caption="Save as", directory="reports",filter="Csv Files (*.csv);;all files(*.*)")

		with open (fileName, 'w') as f:
			f.write("Export date {}\nEvents\n".format(datetime.datetime.now()))
			# Start with a list of Events
			for e in self.ld[param]['eventList']:
				f.write("{}\n".format(e[1]))		#.decode()))
			f.write ("\nChannel\n")
			for tCh in self.ld[param]['channelList']:
				f.write("{}\t".format(tCh['channelNo']))
			f.write("\n")
			f.write("Alias\n")
			for tCh in self.ld[param]['channelList']:
				f.write("{}\t".format(tCh['alias']))
			f.write("\nmax\n")
			for tCh in self.ld[param]['channelList']:
				f.write("{}\t".format(max(tCh['yData'][xmin:xmax])))
			f.write("\nmin\n")
			for tCh in self.ld[param]['channelList']:
				f.write("{}\t".format(min(tCh['yData'][xmin:xmax])))
			f.write("\ndata\n")
			for n in range(xmin, xmax):
				for tCh in self.ld[param]['channelList']:
					f.write("{}\t".format(tCh['yData'][n]))
				f.write('\n')
					
			
	def getSortKey(self, e):
		'''| Return the second key in the list
		| Used for for sort keys
		| Input: e: list to sort '''
		return e[1]
		
	def menuSort(self, channelType, param):
		'''| Sort checkBoxes in the select menu
		| Input ChannelType: 'v' or 't' or 'c' volt, temp or cam'''		
		val = []
		for ch in self.ld[channelType]['channelList']:	# extract all max or min values
			val.append([ch['channelNo'], ch[param]])	
		val.sort(key=self.getSortKey, reverse= ('max'==param))	
		cb = []	# Remove all checkboxes
		while self.ld[channelType]['selectWidget'].count() > 0:
			cb.append(self.ld[channelType]['selectWidget'].takeAt(0))
		for ix in val:
			for ch in self.ld[channelType]['channelList']:
				if ch['channelNo'] == ix[0]:
					self.ld[channelType]['selectWidget'].addWidget(ch['checkBox'])	# Add widget to screen
					break
			
	def menuTempSortMaxSlot(self):
		''' Slot for Temp / Sort / Max '''
		self.menuSort('t', 'max')		

	def menuTempSortMinSlot(self):
		''' Slot for Temp / Sort / Min '''
		self.menuSort('t', 'min')		
	
	def menuTempSortResetSlot(self):
		self.menuSortResetSlot('t')
		
	def menuSortResetSlot(self, channelType):
		'''| Slot for Temp / Sort / Reset
		| Restore all checkboxes to original order'''
		cb = []
		while self.selectTempWidget.count() > 0:	# Remove (but not delete) all Check boxes
			cb.append(self.selectTempWidget.takeAt(0))
		for ch in self.ld[channelType]['channelList']:
			self.ld[channelType]['selectWidget'].addWidget(ch['checkBox'])	# Add widget to screen
	
	def menuCamSortMaxSlot(self):
		''' Slot for Temp / Sort / Max '''
		self.menuSort('c', 'max')		

	def menuCamSortMinSlot(self):
		''' Slot for Temp / Sort / Min '''
		self.menuSort('c', 'min')		
	
	def menuCamSortResetSlot(self):
		self.menuSortResetSlot('c')
		
	def menuCursorMeasureSlot(self, channelType):
		'''| Put a measurement cursor in temp or volt graph
		| Open a data window, dispaying values under cursor'''
				
		if  self.ld[channelType]['sampleCount'] > 0:
			x = self.ld[channelType]['plotWidget'].viewRange()[0] # Note this is not exactly displayed xmin and xmax
			xmin = max (int(x[0]),0)
			xmax = int(x[1])
			xValue = (xmax - xmin)/2 + xmin
			if channelType in ['t', 'c']:
				self.cursorTemp.setValue(xValue)
				self.cursorTemp.show()
				print ("T menuCursorMeasureSlot", channelType)
			if channelType in ['v', ]:
				self.cursorVolt.setValue(xValue)
				self.cursorVolt.show()
				print ("V menuCursorMeasureSlot", channelType)
			#self.cursorWindow.showWin()	
		
	def menuFanOffCompSlot(self):
		pass
	
	def menuDeltaTempCompSlot(self):
		pass
	
	def help(self):
		'''| Slot for Help / Help 
		| Open webbrowser with sphinx documentation'''
		webbrowser.open(os.getcwd()+"/docs/build/html/index.html")
			
	def menuCamComRespons(self):	# To be renamed
			self.camDialog = uic.loadUi('camDialog.ui')
			self.camDialog.buttonBox.clicked.connect(self.camEditDialogRespons) #All buttons in Cam dialog
			self.camDialog.show() 
			
	def setCamAlias(self, camAlias):
		for n, s in camAlias.items():
			for tCh in  self.ld['c']['channelList']: 
				if  str('X'+str(n+1))== tCh['label']:	# Hopefully alias are in order 
					tCh['alias'] = s
			n+=1
	
	def camEditDialogRespons(self,button): # Getting IP address
		if button.text()[-2:] == 'OK':
			self.camCmdQueue = multiprocessing.Queue()
			self.ipAddress = self.camDialog.ipAddressEdit.text()
			self.cam = multiprocessing.Process(target=camera.Camera,args=(self.guiQueue, self.camCmdQueue, self.ipAddress) ) # Process to receive data from
			self.ipLabel.setText(self.camDialog.ipAddressEdit.text())
			self.cam.daemon = True
			self.cam.start()		
	
		
	def menuReportSlot(self):
		self.reportDialog.show()

	def menuRestoreAutoRangeSlot(self):
		self.ld['t']['plotWidget'].getViewBox().enableAutoRange()
		self.ld['v']['plotWidget'].getViewBox().enableAutoRange()
		self.autoPan =False
		self.autoPanCheckBox.setChecked(False)	# Checkbox
		self.ld['t']['forceUpdate']= True
		self.ld['v']['forceUpdate']= True
		
	def menuExportImageSlot(self, channelType, fileName = ''):
		if not fileName:
			options = QFileDialog.Options()
			options |= QFileDialog.DontUseNativeDialog
			fileName, _ = QFileDialog.getSaveFileName(self,caption="Save image as", directory="reports",filter="png Files (*.png);;all files(*.*)")
		if fileName:
			exporter = pg.exporters.ImageExporter(self.ld[channelType]['plotWidget'].plotItem)
			#exporter = pg.exporters.ImageExporter(self.ld[channelType]['plotWidget'].setLimits( xMin = 1,xMax = 10, yMin = 10, yMax = 20)) # self.ld[channelType]['plotWidget'].viewRange()[0]))
			exporter.parameters()['width'] = 1000  
			#exporter.parameters()['height'] = 1000 Not working, locked to width?
			exporter.export(fileName)

	def reportDialogSlot(self, button):	
		self.statusBar().showMessage("Creating reports") 
		if button.text()[-2:] == 'OK':
			options = QFileDialog.Options()
			options |= QFileDialog.DontUseNativeDialog
			filename, _ = QFileDialog.getSaveFileName(self,caption="Save report as", directory="reports",filter="html Files (*.html);;all files(*.*)")
			if filename:
				# Save h5py data? Complete for all types
				if self.reportDialog.checkBoxReportDataSet.isChecked():
					self.saveData(filename.split('.')[0]+'.hdf5')
					
				rp = reportGen.ReportGen(self.ld)	
				page = rp.createPage(self.cameraName,self.ipAddress,self.cameraMacAddress, self.cameraProgVer, self.logDate, self.reportDialog.textEditReport.toPlainText())
				channelTypes=[]
				# Filter voltage type according to checkbox
				for channelType in self.ld:
					channelTypes.append(channelType)
				if not self.reportDialog.checkBoxReportVoltage.isChecked():
					channelTypes.remove('v')
				for channelType in channelTypes:
					la=[]	# Create Summary
					al=[]
					mi=[]
					ma=[]
					no=[]
					if self.ld[channelType]['sampleCount'] > 0:
						#Export CSV data?
						if self.reportDialog.checkBoxReportCsvComplete.isChecked():
							self.csvExportWindow(channelType, 0,  self.ld[channelType]['channelCount'], fileName = filename.split('.')[0]+'_'+self.ld[channelType]['name']+'.csv')
						if self.reportDialog.checkBoxReportCsvWindow.isChecked():
							x = self.ld[channelType]['plotWidget'].viewRange()[0] # Note this is not exactly displayed xmin and xmax
							self.csvExportWindow(channelType, max (int(x[0]),0), int(x[1]), fileName = filename.split('.')[0]+'_'+self.ld[channelType]['name']+'_window.csv')
						# Create summary table
						for tCh in self.ld[channelType]['channelList']:
							#print ("count ",tCh['yData'][self.ld[channelType]['sampleCount']])
							la.append(tCh['label'])
							al.append(tCh['alias'])
							mi.append(tCh['min'])
							ma.append(tCh['max'])
							no.append(tCh['yData'][self.ld[channelType]['sampleCount']-1])
						d = {'label' : la, 'Alias' : al, 'Min' : mi,  'Max' : ma, 'Last' : no}
						rp.createTable(page, "Summary", self.ld[channelType]['name'], 'Logger' , d)
						# Create events
						es = ''
						for e in self.ld[channelType]['eventList']:
							es = es+e[1]+'<br>'
						rp.createText(page,'Summary', self.ld[channelType]['name'],'Events', es)
						# Create plot
						if self.reportDialog.checkBoxReportInteractive.isChecked():
							d={}
							for tCh in self.ld[channelType]['channelList']:
								col = tCh['label'] + ' - ' + tCh['alias']
								d[col] = tCh['yData'][:self.ld[channelType]['sampleCount']] 
								yLabel =  'Volt [v]' if channelType == 'v' else 'Temp [ deg C]'
								htmlCol = 'Temp' if channelType == 'c' else 'Logger'
							rp.createPlot(page, "Plots", self.ld[channelType]['name'], htmlCol, d, yLabel, self.ld[channelType]['xData'][:self.ld[channelType]['sampleCount']] )
						else: # Create static images
							# Complete image
							fName = filename+'_'+channelType+'_complete_.png'
							self.menuExportImageSlot(channelType, fName)
							rp.createImage(page, 'Images', self.ld[channelType]['name'], 'Logger',fName)
				if not self.reportDialog.checkBoxReportInteractive.isChecked(): # Add image description
					desc = {'Color / Pattern' : {'1':'Brown','2':'Red','3':'Orange', '4':'Yellow','5':'Green','6':'Blue','7':'Violet','8':'Gray',
						'A':'Solid','B':'Dash','C':'Dot','D':'Dash dot','X':'Dash dash dot'}}
					rp.createTable(page,'Images','Image codes', '', desc)
				rp.saveReport(page, filename)	
				self.statusBar().showMessage("Reports created")
			else:
				self.statusBar().showMessage("No Reports created, no filename")
		else:
			self.statusBar().showMessage("No Reports created")

	
	def cursorPositionChanged(self, ev):
		''' cb for measurement cursor for both temp and volt graph
		'''
		self.cursorWindow.clearWindow()	
		pos = QCursor.pos()
		widget = QApplication.widgetAt(pos)
		#print (widget.parent().objectName())
		# Get which plot widget has the cursor 
		channelType = None
		if 'tempPlotWidget' == widget.parent().objectName():
			channelType = self.mouseClickedInGraphConnected		# Either 'c' or 't'	
		if 'voltPlotWidget' == widget.parent().objectName():
			channelType = 'v'
		# Find x index from value given in ev
		if channelType:
			xIndex = min(range(len(self.ld[channelType]['xData'])), key=lambda i: abs(self.ld[channelType]['xData'][i] - ev.value() ) ) 
			#y = self.ld['c']['channelList'][0]['yData'][xIndex]
			for channelType in self.ld:
				for tCh in self.ld[channelType]['channelList']:
					txt = '{0:3.2}  {1:6.2f} {2:11.10}'.format(tCh['label'],tCh['yData'][xIndex], tCh['alias'])
					style = "background-color: rgb(" +self.colorListCb[tCh['channelNo']%8] + ")"
					self.cursorWindow.appendValue(channelType, txt, style)		
		
		
	def cursorRemove(self):
		#self.cursor.deleteLater() # Crasch app. See http://enki-editor.org/2014/08/23/Pyqt_mem_mgmt.html
		self.cursorTemp.hide()
		self.cursorVolt.hide()
		self.cursorWindow.clearWindow()
		
class TimeAxisItem(pg.AxisItem):

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		#self.n = QDateTime.currentDateTime()
		self.n = QDateTime.fromSecsSinceEpoch(0)	
		
	def tickStrings(self, values, scale, spacing):	# Illegal warning!
		return ["{}-{:02d}:{:02d}:{:02d}".format(int(value/86400),int(value/3600) % 24, int(value/60) % 60, int(value) % 60) for value in values]	


class CursorMeasure():
	def __init__(self):
		super().__init__()
		self.cursorMeasureWindow = uic.loadUi('cursorMeasureWindow.ui')
		self.cursorMeasureWindow.show()
		#self.cursorMeasureWindow.buttonBox.clicked.connect(self.close) #All buttons in Alias dialog, overriden by next line
		self.cursorMeasureWindow.finished.connect(self.close)
	'''	
	def cursorMeasureWindowResponseSlot(self, button):
		#closing cursor data window and remove cursor from graph
		#mainw.cursorRemove()
		print ('curosr slot')
	'''
	def clearWindow(self):
		while self.cursorMeasureWindow.tempListWidget.count() > 0:	# A fraction of a first lines remain when cleared, may need beautifying
			self.cursorMeasureWindow.tempListWidget.takeAt(0)
		while self.cursorMeasureWindow.voltListWidget.count() > 0:
			self.cursorMeasureWindow.voltListWidget.takeAt(0)

	
	def appendValue(self, channelType, valueStr, style):
		if channelType in ['t', 'c']:
			widget = self.cursorMeasureWindow.tempListWidget
			
		if channelType == 'v':
			widget = self.cursorMeasureWindow.voltListWidget
	
		l = QLabel(valueStr)
		l.setStyleSheet(style)
		widget.addWidget(l)
	
	def hideWin(self):
		self.cursorMeasureWindow.hide()
		
	def showWin(self):
		self.cursorMeasureWindow.show()
		
	def close(self):	
		try:
			mainw.cursorRemove()
		except:
			print ("close cursor measurement window exception")
	
		
if __name__ == '__main__':         
	app = QtWidgets.QApplication(sys.argv)
	mainw = MainWindow()
	mainw.show()
	sys.exit(app.exec_())

