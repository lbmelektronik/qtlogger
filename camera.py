

import re
import socket
import time
import requests
import sshmanager

class Camera():

    def __init__(self, guiQueue, camCmdQueue, ipAddress, *args, **kwargs):
        self.workerqueue = guiQueue
        self.camCmdQueue = camCmdQueue
        self.ipAddress = ipAddress
        self.get_temps_from_cam()
        
    def get_sensor_names(self):
        """
        Read temperature sensor names from camera.

        This function makes an ssh connection to the camera and reads
        out available temperatures labels.
        | Return list of labels
        """
        labels = {}
        stdout, __ = self.ssh.execute(self.ipAddress, "temperature_status.sh")
        temp = stdout.split("\n")
        channel = 0
        cont = True
        while cont:
            for line in temp:
                pattern = "^Sensor S%d: (.*) C" % channel
                match_object = re.search(pattern, line, re.MULTILINE)
                if match_object is None:
                    cont = False
                    break
                url = "http://{}/axis-cgi/param.cgi?action=list".format(
                    self.ipAddress
                ) + "&group=root.TemperatureControl.Sensor.S{}.Name".format(
                    channel
                )
                resp = requests.get(
                    url, auth=requests.auth.HTTPDigestAuth("root", "pass")
                )
                labels[channel] = resp.content.decode("utf-8").split("=")[1][
                    :-1
                ]
                channel += 1
        return labels

    def get_temps_from_cam(self):
        """Read temperatures from the camera's internal sensors."""
        # i = 0
        # simulations = 60 * 60 * 24 * 7 / 2
        # while i < simulations:
        #     self._get_dummy_data((simulations - i)*2)
        #     i = i + 1
        #     time.sleep(0.005)
        self.internal_run = True
        self.internal_interval = 1
        while self.internal_run:
            self.ipAddress    # Init socket
            self.ssh = sshmanager.SSHManager()
            self.internal_labels = (self.get_sensor_names())
            #print ("init socket ready")
 
            while self.internal_run: 
                begin_time = time.time()
                temp = "not connected"
                try:
                    stdout, ___ = self.ssh.execute(
                        self.ipAddress, "temperature_status.sh"
                    )
                    temp = stdout.split("\n")
                    meas_time = time.time()
                    sensors = {}
                    timestamps = {}
                    for line in temp:
                        if line.startswith("Sensor"):
                            key = int(line.split("Sensor S")[1].split(":")[0])
                            val = line.split(": ")[1][:-2]
                            if val[:4] == "-999" or val[:3] == "999":
                                val = "not connected"

                            if key in self.internal_labels:
                                # Not To conform with picologgers:
                                sensors[self.internal_labels[key]] = val
                                timestamps[self.internal_labels[key]] = meas_time
                    self.workerqueue.put(["internal temps", sensors, timestamps])
                except Exception as error:
                    print ("ssh error", error)
                    break
                while self.internal_interval - (time.time() - begin_time) > 0.05:
                    time.sleep(0.05)
                
                while not self.camCmdQueue.empty():
                    command = self.camCmdQueue.get()
                    if command == 'getAlias':
                        self.workerqueue.put(["camAlias", self.internal_labels])
                        
                    if command == 'setSampleTime':
                        self.internal_interval = command[1]
                    
                    self.internal_run = not (command == 'exit')

                    if command == 'getMacAndId':
                        self.workerqueue.put(['getMacAndId', self.getMacAndProdId(self.ipAddress)])
                                              
    def getMacAndProdId(self,ipAddress):
        url = 'http://' + ipAddress + '/axis-cgi/admin/param.cgi?action=list&group=Brand.ProdNbr'
        try: 
            name=''
            resp = requests.get(url, auth=requests.auth.HTTPDigestAuth("root", "pass")    )
            response = resp.text.split('=')
            if response[0]=='Brand.ProdNbr':
                name = response[1][:-1]   
             
        except:
            print ('get prod nr ssh error')
        
        url = 'http://' + ipAddress + '/axis-cgi/admin/param.cgi?action=list&group=Network.eth0.MACAddress'
        try: 
            mac=''
            resp = requests.get(url, auth=requests.auth.HTTPDigestAuth("root", "pass")    )
            response = resp.text.split('=')
            if response[0]=='Network.eth0.MACAddress':
                mac = response[1][:-1]   
             
        except:
            print ('get mac ssh error')
            
        url = 'http://' + ipAddress + '/axis-cgi/admin/param.cgi?action=list&group=Properties.Firmware.Version'
        try: 
            progVer=''
            resp = requests.get(url, auth=requests.auth.HTTPDigestAuth("root", "pass")    )
            response = resp.text.split('=')
            if response[0]=='Properties.Firmware.Version':
                progVer = response[1][:-1]   
             
        except:
            print ('get prog ver ssh error')
        return mac, name, progVer
    
