
import numpy as np
import esparto as es
#import plotly.graph_objects as go
import plotly.express as px
import pandas as pd
## install pip install plotly==5.3.1

toNSec = 1e9    #From sec

# Page layout hierarchy:
# Page -> Section -> Row -> Column -> Content

# 2000 samples * 40 channels
## Bokeh: html page = 43MB
## Plotley: html page = 0.6MB

class ReportGen():
    def __init__(self, *args, **kwargs):
        pass
        
    def createPage(self, product, ipAddress, macAddress, progVer, logDate, note):
        page = es.Page(title="tvLogger test report")
        page[product]["Test object"]["Log date"] = logDate
        page[product]["Test object"]["Ip address"] = ipAddress
        page[product]["Test object"]["Program version"] = progVer
        page[product]["Test object"]["Mac address"] = macAddress
        page[product]["User note"][""] = note
        self.product = product
        return page
        
    def createTable (self, page, section, row, col, table):
        df = pd.DataFrame (table, columns = list(table.keys()))
        df.set_index(list(table.keys())[0])
        page[section][row][col] = df
        
    def createPlot(self, page, section, row, col, graph, yLabel, xaxis):
        x = pd.to_datetime(np.multiply(xaxis,toNSec) )  # x axis convert from seconds to nano seconds
        pd.options.plotting.backend = "plotly"
        df = pd.DataFrame (graph, columns = list(graph.keys()), index = x)
        #fig = df.plot(labels=dict(index="Time", value=yLabel, variable="Channel"))
        fig = px.line(df, labels=dict(index="Time", value=yLabel, variable="Channel"))
        fig.update_layout(xaxis_tickformatstops = [
            dict(dtickrange=[None,3600], value="%M:%S s"),
            dict(dtickrange=[3600, 36000], value="%H:%M m"),
            dict(dtickrange=[36000, None], value="%d-%H h")])
        #fig.update_layout(xaxis=dict(rangeslider=dict(visible=True))) # Slider removes Y zoom :(
        page[section][row][col]= fig
        
    def createImage (self, page, section, row, col, fileName):
        page[section][row][col]= fileName
          
    def createText (self, page, section, row, col, text):
        page[section][row][col]= text

    def saveReport(self, page, fileName):
        # Add File dialog
        page.save_html(fileName)
          
if __name__ == '__main__':         
    # test data
    product = "Q3536-LVE"
    ipAddress = "192.168.0.90"
    macAddress = "00408c1234"
    rg = ReportGen()
    rg.createPage(product, ipAddress, macAddress)
    rg.saveReport()
    
    