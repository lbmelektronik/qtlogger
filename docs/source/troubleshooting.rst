Troubleshooting 
---------------------

Temperature sensor not listed?
""""""""""""""""""""""""""""""""""""""""""""""""""""""""
Sensor is either shorted or open circuit. Shorted can happen if the sensor is placed between PCB and heat sink. Open can be wrong sensor selected, a broken PCB, bad soldering. Also check the FPC is correct inserted in the connector. Version 1 logger; check that the FPC adapter is correct mounted on the pin list.

Logger doesn't start   
""""""""""""""""""""""""""""
If the tvLogger is restarted, the logger may have problem to syncronize the communication. Restart tvLogger application *and* logger hardware 

Load Alias dialog doesn't show up
""""""""""""""""""""""""""""""""""""""""""""""""""""""""
Check program version of the logger should be at least 2.5.3

Low sample rate on logger
""""""""""""""""""""""""""""
Old loggers may have an early program version, conversion time for all channels ~6 seconds. Latest firmware is below 1 second. Program version should be at least 2.5.3

I can't manipulate the aliases in the logger
""""""""""""""""""""""""""""""""""""""""""""""""""""""""
No, i't not aimed for that. But if yuo open the USB memory in the logger (close tvLogger, power cycle the logger with with the button pressed. Release the button as soon as the LED start flashing). 

File ALIAST.APD for temperature and ALIASV.APD for voltage.

Example: {"A4": "CPU temp", "B1": "brown", "B2": "-", "B3": "-", "B4": "-", "B5": "-", "B6": "-", "B7": "-", "B8": "-"}

Keep formating. No local characters and use editor that to not add extra line feeds (use unix mode if available).