
PT1000 Logger
=============
As the name imply, using linear PTC elements for temperature readings.

Pt1000 logger version 2
-----------------------
With FPC connectors for temperatures and beige vertical connectors for voltages

.. image:: images/pt1000_2_pinout.png
   :width: 800

For channel 1-8 use either pin list **OR** FPC connector, not both. For this reason only PT1000 elements are accepted on these decks.

.. _block_description:

* Block 1  Connector X5
    - Pin 1 Channel 1
    - Pin 8 Channel 8
    - Pin 10 GND
    - Use this block for the main board
* Block 2  Connector X7
    - Pin 1 Channel 9
    - Pin 7 Channel 15
    - Pin 8 Block 2 Virtual GND
    - Pin 10 GND (do not use in multi block measurement)
    - Use this block for a sub unit
* Block 3  Connector X8
    - Pin 1 Channel 17
    - Pin 7 Channel 23
    - Pin 9 Block 3 Virtual GND
    - Pin 10 GND (do not use in multi block measurement)
    - Use this block for a sub unit
* Block 4  Connector X6
    - Pin 1 Channel 25
    - Pin 7 Channel 31
    - Pin 8 Block 4 Virtual GND
    - Pin 10 GND (do not use in multi block measurement)
    - Use this block for a sub unit

**Note: connect only one GND to DUT and select the one with lowest potential on DUT (main).
It's recommended to use GND on Block1. Use Virtual ground VGND for the other blocks to avoid current loops.**
VGND is an analog input used for differential input. VGND is separate for each connector.

The voltage logger is aimed for low impedance signals, voltage regulators, current sense etc. The input impedance is 260kΩ.

Maximum input voltage +25V, positive signals only. Keep virtual GNDs positive.

Pt1000 logger version 1
-----------------------
With pin lists for temperatures and black angled connectors for voltages

.. image:: images/pt1000_1_doc.png
   :width: 800

:ref:`Pinout and description is same as logger version 2<block_description>`

Accuracy
--------
Logger version 1 and 2 have the same accuracy.

Temperature accuracy
^^^^^^^^^^^^^^^^^^^^^^
Temperature range -70 to +150 °C (PT500 max 110 °C)

.. image:: images/summary.svg
   :width: 600

* Reference error: Reference resistor offset error
* Coeff error: Reference resistor temperature error
* Calc error: Error in algoritm (c-code)
* Self heat: Power created by measurement current and PTC resistance
* PT1000: sensor absolute error, two versions available; ±0.15°C or ±0.3°C

The logger automatically detect PT100, PT500 or PT1000 elements.

Reference for calculations
`UliEngineering <https://techoverflow.net/2016/01/02/accurate-calculation-of-pt100pt1000-temperature-from-resistance/>`_
implements a polynomial-fit based algorithm to provide 58.6u°C peak-error over the full defined temperature range from -200 °C to +850 °C.



Voltage accuracy
^^^^^^^^^^^^^^^^^^

Input voltage dividers
"""""""""""""""""""""""
* Resistor accuracy Max ±0.05% error * 2 resistors
* Temperature coefficient 10ppm/°C  → High and Low resistors are same brand and type. Expect the same change in high and low resistor, therefore very low additional error 

Reference error
"""""""""""""""
* Voltage error max ±0.02%
* Temp coefficient
   * 1 ppm/°C 0 - 70°C 
   * 2 ppm/°C over temp  → ±75°C  →  ±0.015%


Communication protocol
----------------------
Baud rate 115200

.. list-table:: Commands
   :widths: 12, 12, 12, 12, 52
   :header-rows: 1

   * - Command
     - On
     - Off
     - Single shot
     - Comment
   * - Temperature
     - $TE1@
     - $TE0@
     - $TES@
     -
   * - Voltage
     - $VO1@
     - $VO0@
     - $VOS@
     -
   * - Raw Data
     - $RA1@
     - $RA0@
     - $RAS@
     - Debug, A/D in Hex. Not fully impl. 	 	 
   * - Temp Channel Enable
     - $CHT011@
     - $CHT320@
     - $CHTxxy@ 
     - xx 2 digit channel number 01-32, y =0 -> disable, y=1 -> enable
   * - Voltage Channel Enable
     - $CHV011@
     - $CHV320@
     - $CHTxxy@
     - xx 2 digit channel number 01-32, y =0 -> disable, y=1 -> enable
   * - All Temperatures Enable
     - $CHTA1@
     - $CHTA0@
     -
     -
   * - All Voltage Enable
     - $CHVA1@
     - $CHVA0@
     -
     -
   * - Relay Enable
     - $RE1@
     - $RE0@
     -
     -
   * - Heater Enable
     - $HE1@
     - $HE0@
     - $HEA@ 
     - Automatic heater control (default)

   * - File Write
     - $FWxxxxyyyyyyyy@
     -
     -
     - Max 2k data. No extension. Save to USB mem.
   * - File Read
     - $FRyyyyyyyy@
     - 
     -
     - Data returned if file exist
   * - File erase
     - $FEyyyyyyyy@
     -
     -
     -
   * - Program version
     - $PR@
     -
     -
     - Return program version and serial number

Note, heater is not working in Logger version 2 (hw)

File access only for small files e.g config files.


Output
^^^^^^^^^
The output from the logger is in CSV format:
T25.27 ,999.00,999.00,999.00,999.00,999.00,999.00,999.00,999.00,999.00,999.00,999.00,999.00,999.00,999.00,999.00,
999.00,999.00,999.00,999.00,999.00,999.00,999.00,999.00,999.00,999.00,999.00,999.00,999.00,999.00,999.00,999.00

V0.0000 ,0.0000 ,0.0000 ,0.0000 ,0.0000 ,0.0000 ,0.0000 ,0.0000 ,0.0000 ,0.0003 ,0.0003 ,0.0003 ,0.0003 ,0.0003 ,0.0003 ,0.0000 ,
0.0000 ,0.0000 ,0.0000 ,0.0000 ,0.0000 ,0.0000 ,0.0000 ,0.0000 ,0.0000 ,0.0000 ,0.0000 ,0.0000 ,0.0000 ,0.0000 ,0.0000 ,0.0000

Initial T character for Temperatures and V for Voltages.

Channels not enabled return NaN, Not a Number:
T999.00,NaN ,NaN ,NaN ,NaN ,NaN ,NaN ,NaN ,NaN ,NaN ,NaN ,NaN ,NaN ,NaN ,NaN ,NaN ,
999.00,NaN ,NaN ,NaN ,NaN ,NaN ,NaN ,NaN ,NaN ,NaN ,NaN ,NaN ,NaN ,NaN ,NaN ,NaN

Temperature channels detect open and short connection, short returns -99 deg C and open (not connected) +999 deg C.

Relay
----------------------
A relay is available for DUT power control. Connect to X4 pin 9.10. Max switch 170V, 0.5A or 10W.

Heater
----------------------
An integrated 0.5W heater is used to increase the low end temperature range.

Target power
----------------------
Logger Version 1
^^^^^^^^^^^^^^^^
The logger can be powered from target, connect X4 pin 1 to +3.3V and pin 8 to GND. This require off line logging software (work in progress).

Logger Version 2
^^^^^^^^^^^^^^^^
Connect 5V  to USB connector pin 1 VCC and 5 GND


 