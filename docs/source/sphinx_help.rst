Managing documentation for tvLogger
-----------------------------------


* Create / Update
 
  Sphinx is used to create html help pages. URL: `Sphinx <http://www.sphinx-doc.org>`_.

 * Install Sphinx: 
   
    .. code-block:: python

        pip install Sphinx 

 * Configure Sphinx: sphinx-quickstart. It create a source directory with conf.py and a master document, index.rst (if you accepted the defaults). 
 * Conf.rst is manually modified: 
 
   * html_theme = 'classic'
   * show_authors = False 
   * todo_include_todos = True
   * extensions = 
   
     * ['sphinx.ext.autodoc',
     * 'sphinx.ext.todo',
     * 'sphinx.ext.viewcode',]
     
 * Build: 'sphinx-build -b html sourcedir builddir'. Or just 'make html' in the doc directory
 * All Sphinx source files are located in 'doc' directory
 
 **Note, if using subdirectories such as pyTools/script/myTool, where script dir does not contain any .py files,
 the documentation chain will break and no myTool files will be documented. 
 Add an empty (insert a note why it exist) __init__.py (2 underscores) file in the script dir. 
 This is required in every sub directory with or without .py files.** 
 
* Example of markups in the .rst file:
  
 * \.. note\::
 * \.. warning\::
 * \.. versionadded:: version
 * \.. versionchanged:: version
 * \.. seealso\::
 * highlight\:: <NL,NL> ::
 
     highlighted text

* Example of inline markup

 * one asterisk: \*text* for emphasis (italics),
 * two asterisks: \**text** for strong emphasis (boldface), and
 * backquotes: \``text`` for code samples.
 * markup symbols in text: precede with \\

Section headers
^^^^^^^^^^^^^^^
 
*  # with overline, for parts
*  \* with overline, for chapters
*  =, for sections
*  -, for subsections
*  ^, for subsubsections
*  ", for paragraphs

Adding new tool modules to help system
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Execute:: 

   sphinx-apidoc -o doc source

The new .rst file is named scripts.new_script and is included in the doc directory. Also check the doc/modules.rst file.  

Then::

  make clean
  make html
   
