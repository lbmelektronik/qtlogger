Install tvLogger
==========================

Requirements
--------------
* Linux or Windows os
* Python 3
* PyQt5
* PyQtGraph
* HDF5 (h5py serialization)
* Paramiko
* Sphinx (help & documentation)
* Pandas 
* Esparto report generator
* Plotly

Install manually
-----------------
* pip install PyQt5
* pip install h5py
* pip install pyqtgraph
* pip install paramiko
* pip install esparto
* pip install pandas
* pip install plotly
* Create tvLogger directory
* Unzip tvLogger.zip to program directory
* Create directory tvLogger/data
* Create directory tvLogger/reports

Automatic installation
--------------------------
TBD


