 
Quickstart the tvLogger
========================
User interface
--------------
* You can zoom and pan in the graphs with the mouse.
* Use the scrollbar to the right of the graphs to change the relative size between temperature and voltage graph
* Logging can be paused or reset
* Temperature and voltage x axes can be syncronized. Temperature area is master.
* You can choose black or white graph background.
* Temperature channels are named A1-D8, flex PCB A to D. Channel 1 to 8 colored according to "resistor color codes"
* Internal camera temperatures are named X
* Logging data can be saved and loaded back
* Data can be csv exported, part of or all data
* If a channel id disabled, logging continue in background including max - min calculation
* Auto backup. Logging data is backed up continuously
* Sample time can be set 1 - 10 seconds
* Events (markers) can be added to the plot
* Measurement cursor
* HTML report, including interactive graphs. PDF export

Temperature
------------
* Connect the sensors to the target. 
   * Start with a plan of which sensors goes where for best cabeling and connection to the logger
   * Peel of the protective tape on the back of the sensor and mount on DUT. Use glue if necessary
   * Make a note of sensor number, DUT position and flex pcb
* Connect the flex pcbs to the logger. Note that the latch is open in the "outer" (away from flex) position. When logger connected to the PC.
* Start the logger. Temperatures are automatically detected and enabled. Expected sensors not showing up are either shorted or have open circuit for some reason.
* Edit channel alias by right mouse click on channel. When ready select File / Save alias to logger. When the logger is moved to another station, the alias can be loaded to the PC. Common command for Temp and Volt.


Zoom / Pan
--------------
Zoom with wheel in graph, on x or y axis. Or drag Right mouse button in graph.

Pan with Left mouse button

Voltage
------------
Since there is no way to detect if the channel is connected, all channels are logged. 

Save and load data
-------------------
Use *File / Save* to store the complete logging to disk. All files stored in 'data' directory
Use *File / Load* to post evaluate the logging, e.g. csv export. 

Common command for all data, Temp, Volt and Camera

Backup
----------------
Backup to disk is done every 10 minutes. Two log files are used. Restore from newest file
Use Load to restore auto backuped files. 

Export csv data
----------------
Export data can either be for a selected area or the whole execution. To export part of the data, scale the window to the actual time frame and select window export.

Events
----------------
You can add an marker / event to a plot, an vertical infinite line with label. All events are listed in the Events tab. 

To add an event press middle button (wheel) in a graph, add descriptive text in the popup and press OK. The event will be added at the time under cursor.

Events are saved during save-data, backup and csv export.

Reports
--------
Create a set of reports.

* CSV complete or window
* Complete data set, tvLogger format, for post evaluation of logging data, e.g. measure in graph, export data etc
* HTML document with interactive graphs, Make it possible to pan, zoom and measure in graph. Single HTML file. PDF if selected
* Add custom note

For very long graphs, disable the interactive plots and generate an image instead. "Very long": how many channels, camera- temperature- voltage-log, sample time, power in the PC etc.

Report files are stored in ../reports directory

In the interactive graphs, toggle line on-off by clicking on the legend. Double click to de-select all others. Zoom all double click in graph.

Measurement Cursors
-------------------
Cursors can be used to list all values at a certain point in time. Values are listed in separate window.

Menu *Temperature / Cursor measure* or *Voltage / Cursor measure* to activate cursor in temperature or voltage graph.

Drag the cursor with left mouse button.

The cursors are removed when the data window is closed.

Tip 1: It can be hard to catch the cursor in running log. Use pan or zoom to freeze the graph. Avoid auto pan.

Tip 2: Avoid using both cursors at the same time, it's confusing where the values come from.

Tip 3: The cursor may with manual pan or autopan be moved outside the graph. Turn off autopan and press *File / Restore auto range* to find the cursor.



