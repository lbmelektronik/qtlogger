.. tvLogger documentation master file, created by
   sphinx-quickstart on Thu Apr  1 19:45:44 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to tvLogger
===================
| tvLogger is a Python app for Temperature and Voltage logging.
| 32 channels temperature -70 to +150 °C
| 29 channels voltage 0 to 25 volt 
| Internal temperatures from Axis cameras

Running on both Linux and Windows systems (with same code base)

Contents:
=========
.. toctree::
   :numbered:
   :maxdepth: 3

   installation
   quickstart
   pt1000Logger
   troubleshooting
   modules
   sphinx_help
   
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
