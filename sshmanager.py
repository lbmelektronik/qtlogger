import paramiko

class SSHManager():
    def __init__(self):
        pass
    
    def execute(self, ipAdress, cmd):
        ssh_client = paramiko.SSHClient()
        ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh_client.connect(hostname=ipAdress,
                           username='root',
                           password='pass')
        stdin, stdout, stderr = ssh_client.exec_command(cmd)
        out = stdout.read().decode().strip()
        ssh_client.close()
        return (out, None)