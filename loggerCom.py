import serial.tools.list_ports
import time
import sys

class SerialCom():    
    def __init__(self, guiQueue, serialComQueue):
        self.serialComQueue = serialComQueue
        self.guiQueue = guiQueue
        super(SerialCom, self).__init__()
        self.counter = 0
        self.processData()
    '''

    '''
    def openPort(self):
        try:
            if self.pt1000:
                return True        # Port already configured
        except:
            try:
                plist = list(serial.tools.list_ports.comports())
                for e in plist:
                    if str(e.hwid).split()[1].split('=')[1] == '0483:5740':
                        port = str(e).split()[0]
                        break
                self.guiQueue.put(["msg", "Waiting for USB port " +str(self.counter) + "/100"])                
                self.pt1000 = serial.Serial(port, 115200,timeout=7)
            except:
                self.counter += 1
                time.sleep(1)
                if self.counter > 100: 
                    self.guiQueue.put(["msg", "No response from Logger!"])
                    sys.exit()
                    return False
            else:
                self.guiQueue.put(["msg", "Logger connected to " + port])
                self.pt1000.write (b'$TE1@')    #Start temperature logging
                s = self.pt1000.readline()    # Read ack
                if 0==len(s):
                    self.guiQueue.put(["msg","Error: Serial com timeout"])
                    return False
                self.pt1000.write (b'$VO1@')    # Start voltage logging
                s = self.pt1000.readline()    # Read ack
                return True

    '''
    Read serial port and send to main process
    '''            
    def processData(self):
        while True:
            if  self.openPort():
                s = self.pt1000.readline()[:-1] # Read data. Remove ending CR
                if len(s) > 10: # data string (no ack data)
                    dataStr = "".join([chr(_) for _ in s]) # bytes -> string
                    #print ("data from Logger")
                    self.guiQueue.put(["plot", dataStr])
            while not self.serialComQueue.empty():
                command = self.serialComQueue.get()
                #print ("send -> ", command[0])
                if command[0] == 'exit':
                    self.pt1000.write (b'$TE0@')    # Turn off Logger
                    s = self.pt1000.readline()    # Read ack
                    return
                if command[0] == 'logSaveTempAlias':
                    self.pt1000.write (b'$TE0@')
                    self.pt1000.write (b'$VO0@')
                    time.sleep (0.5)
                    self.pt1000.flushInput()
                    l = len(command[1])
                    if l < 2000:
                        self.pt1000.write(bytes ('$FW{:04d}ALIAST@'.format(l), 'utf-8'))
                        self.pt1000.write(str(command[1]).encode('utf-8'))
                        self.guiQueue.put(['msg', "Temp alias saved to logger"])
                    self.pt1000.write (b'$TE1@')
                    self.pt1000.write (b'$VO1@')
                    time.sleep(0.5)
                    self.pt1000.flushInput()
                if command[0] == 'logReadTempAlias':
                    self.pt1000.write (b'$TE0@')
                    self.pt1000.write (b'$VO0@')
                    time.sleep (0.5)
                    self.pt1000.flushInput()
                    self.pt1000.write(b'$FRALIAST@')
                    d = self.pt1000.readline()
                    self.guiQueue.put(["tempAlias", d])
                    self.pt1000.write (b'$TE1@')
                    self.pt1000.write (b'$VO1@')
                    time.sleep(0.5)
                    self.pt1000.flushInput()
                if command[0]=='logRemoveTempAlias':
                    self.pt1000.write (b'$TE0@')
                    self.pt1000.write (b'$VO0@')
                    time.sleep(0.5)
                    self.pt1000.flushInput()
                    self.pt1000.write(b'$FEALIAST@')
                    d = self.pt1000.readline()
                    self.guiQueue.put(["msg", d.decode()])
                    self.pt1000.write (b'$TE1@')
                    self.pt1000.write (b'$VO1@')
                    time.sleep(0.5)
                    self.pt1000.flushInput()
                    
                if command[0] == 'logSaveVoltAlias':
                    self.pt1000.write (b'$TE0@')
                    self.pt1000.write (b'$VO0@')
                    time.sleep(0.5)
                    self.pt1000.flushInput()
                    l = len(command[1])
                    if l < 2000:
                        self.pt1000.write(bytes ('$FW{:04d}ALIASV@'.format(l), 'utf-8'))
                        self.pt1000.write(str(command[1]).encode('utf-8'))
                        self.guiQueue.put(['msg', "Volt alias saved to logger"])
                    self.pt1000.write (b'$TE1@')
                    self.pt1000.write (b'$VO1@')
                    time.sleep(0.5)
                    self.pt1000.flushInput()
                if command[0] == 'logReadVoltAlias':
                    self.pt1000.write (b'$TE0@')
                    self.pt1000.write (b'$VO0@')
                    time.sleep(0.5)
                    self.pt1000.flushInput()
                    self.pt1000.write(b'$FRALIASV@')
                    d = self.pt1000.readline()
                    self.guiQueue.put(["voltAlias", d])
                    self.pt1000.write (b'$TE1@')
                    self.pt1000.write (b'$VO1@')
                    time.sleep(0.5)
                    self.pt1000.flushInput()
                if command[0]=='logRemoveVoltAlias':
                    self.pt1000.write (b'$TE0@')
                    self.pt1000.write (b'$VO0@')
                    time.sleep(0.5)
                    self.pt1000.flushInput()
                    self.pt1000.write(b'$FEALIASV@')
                    d = self.pt1000.readline()
                    self.guiQueue.put(["msg", d.decode()])
                    self.pt1000.write (b'$TE1@')
                    self.pt1000.write (b'$VO1@')
                    time.sleep(0.5)
                    self.pt1000.flushInput()
                    
